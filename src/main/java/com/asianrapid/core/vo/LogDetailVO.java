package com.asianrapid.core.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description: 日志记录详细类
 */
public class LogDetailVO implements Serializable {

	private static final long serialVersionUID = -3159010329852720643L;

	private Integer id;

	/**
	 * 服务接口ID
	 */
	private Integer logId;

	/**
	 * 调用结果：成功/失败
	 */
	private Integer status;

	/**
	 * 记录说明
	 */
	private String operation;

	/**
	 * 日志创建时间
	 */
	private Date createTime;

	/**
	 * 日志更新时间
	 */
	private Date updateTime;

	private String code;

	private String message;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLogId() {
		return logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
