
package com.asianrapid.core.vo;

import java.io.Serializable;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description:
 */
public class JsonSupBean implements Serializable {

	private static final long serialVersionUID = 732066568058880275L;

	public JsonSupBean() {
	}

	private Integer	size;
	private Integer	page;

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

}
