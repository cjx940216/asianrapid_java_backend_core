
package com.asianrapid.core.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description: api日志记录类
 */
public class ApiLogVO extends JsonSupBean implements Serializable {

	private static final long serialVersionUID = 821972097576524979L;
	private String id;
	private String user_id;
	private String service_id;
	private String service_url; // 服务URL
	private String request;
	private String operation;
	private String status;
	private String create_time;
	private String type_id;
	private String service_name; // 服务名称
	private String ip; // 服务调用IP
	private String history; // 时长
	private String update_time;
	private List<LogDetailVO> detail;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getType_id() {
		return type_id;
	}

	public void setType_id(String type_id) {
		this.type_id = type_id;
	}

	public List<LogDetailVO> getDetail() {
		return detail;
	}

	public void setDetail(List<LogDetailVO> detail) {
		this.detail = detail;
	}

	public String getService_id() {
		return service_id;
	}

	public void setService_id(String service_id) {
		this.service_id = service_id;
	}

	public String getService_url() {
		return service_url;
	}

	public void setService_url(String service_url) {
		this.service_url = service_url;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

}
