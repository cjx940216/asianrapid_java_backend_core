package com.asianrapid.core.utils;

import com.asianrapid.core.domain.AsianResult;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description: api结果返回工具类
 */
public class ResultUtils {

    public static AsianResult success(Object object) {
        AsianResult result = new AsianResult();
        result.setCode(0);
        result.setMsg("成功");
        result.setData(object);
        return result;
    }

    public static AsianResult success() {
        return success(null);
    }

    public static AsianResult error(Integer code, String msg) {
        AsianResult result = new AsianResult();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
}
