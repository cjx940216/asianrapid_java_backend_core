package com.asianrapid.core.utils;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description:excel操作工具类（待完善） 封装对excel的操作，包括本地读写excel和流中输出excel,支持office 2007。<br/>
 * 依赖于poi-3.9-20121203.jar,poi-ooxml-3.9-20121203.jar,poi-ooxml-schemas-3.9-
 * 20121203.jar,dom4j-1.6.1.jar<br/>
 * 有参构造函数参数为excel的全路径<br/>
 */
public class ExcelFileUtils {

    // 写入excel时，是否自动扩展列宽度来符合内容。
    private boolean autoColumnWidth = false;

    /**
     * Config工作表的删除
     *
     * @param workbook 工作表
     */
    public static void deleteConfigSheet(Workbook workbook) throws Exception {

        workbook.removeSheetAt(workbook.getSheetIndex("Config"));
    }

    /**
     * excel的header的设定。
     *
     * @param workbook 工作表
     * @param dataRow
     */
    public static void setHeader(Workbook workbook, Map dataRow) throws Exception {

        Map<String, String> templateCongigMap = getTamplateConfigMap(workbook);

        Sheet sheet = workbook.getSheetAt(0);


        int headerRowNumberFrom = Integer.parseInt(templateCongigMap.get("HEADER")) - 1;
        int headerRowNumberTo = Integer.parseInt(templateCongigMap.get("DETAIL"));

        for (int i = headerRowNumberFrom; i < headerRowNumberTo; i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            int lastColumn = row.getLastCellNum();
            for (int j = 0; j < lastColumn; j++) {
                Cell cell = row.getCell(j);
                if (cell == null) {
                    continue;
                }

                setCellValue(cell, dataRow);
            }
        }
    }

    /**
     * footer的设定
     *
     * @param workbook
     * @param dataRow
     */
    public static void setFooter(Workbook workbook, Map dataRow) throws Exception {

        Map<String, String> templateCongigMap = getTamplateConfigMap(workbook);

        Sheet sheet = workbook.getSheetAt(0);

        int footerRowNumberFrom = Integer.parseInt(templateCongigMap.get("FOOTER")) - 1;
        int footerRowNumberTo = sheet.getLastRowNum() + 1;

        for (int i = footerRowNumberFrom; i < footerRowNumberTo; i++) {
            Row row = sheet.getRow(i);
            if (row != null) {
                int lastColumn = row.getLastCellNum();
                for (int j = 0; j < lastColumn; j++) {
                    Cell cell = row.getCell(j);
                    if (cell == null) {
                        continue;
                    }

                    setCellValue(cell, dataRow);
                }
            }
        }
    }

    /**
     * 多个header设定
     *
     * @param workbook  工作表
     * @param sheet
     * @param configMap 配置
     * @param dataRow   数据
     */
    public static void setHeaders(Workbook workbook, Sheet sheet, Map<String, String> configMap, Map dataRow) throws Exception {

        int headerRowNumberFrom = Integer.parseInt(configMap.get("HEADER_ROW_FROM")) - 1;
        int headerRowNumberTo = Integer.parseInt(configMap.get("HEADER_ROW_TO")) - 1;

        for (int i = headerRowNumberFrom; i <= headerRowNumberTo; i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                continue;
            }

            int headerCellNumberFrom = Integer.parseInt(configMap.get("HEADER_CELL_FROM")) - 1;
            int headerCellNumberTo = Integer.parseInt(configMap.get("HEADER_CELL_TO")) - 1;
            for (int j = headerCellNumberFrom; j <= headerCellNumberTo; j++) {
                Cell cell = row.getCell(j);
                if (cell == null) {
                    continue;
                }

                setCellValue(cell, dataRow);
            }
        }
    }

    /**
     * 多个工作表的Footers的设定。
     *
     * @param workbook  工作表
     * @param sheet
     * @param configMap
     * @param dataRow
     * @param list
     */
    public static void setFooters(Workbook workbook, Sheet sheet, Map<String, String> configMap, Map dataRow, List<?> list) throws Exception {

        int footerRowNumberFrom = Integer.parseInt(configMap.get("DETAIL_ROW_FROM")) - 1 + list.size();
        int footerRowNumberTo = footerRowNumberFrom + 1;

        for (int i = footerRowNumberFrom; i < footerRowNumberTo; i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                continue;
            }
            int lastColumn = row.getLastCellNum();
            for (int j = 0; j < lastColumn; j++) {
                Cell cell = row.getCell(j);
                if (cell == null) {
                    continue;
                }

                setCellValue(cell, dataRow);
            }
        }
    }

    /**
     * 详细行的设定
     *
     * @param workbook 工作表
     * @param list     数据
     */
    public static void setDetail(Workbook workbook, List<Map> list) throws Exception {

        Map<String, String> templateCongigMap = getTamplateConfigMap(workbook);

        Sheet sheet = workbook.getSheetAt(0);

        int listSize = list.size();

        for (int i = 0; i < listSize; i++) {

            Map dataRow = list.get(i);

            int currentRowNumber = Integer.parseInt(templateCongigMap.get("DETAIL")) - 1 + i;

            ExcelFileUtils.copyRowToUnder(sheet, sheet.getRow(currentRowNumber));

            Row row = sheet.getRow(currentRowNumber);
            if (row == null) {
                continue;
            }

            int lastColumn = row.getLastCellNum();
            for (int j = 0; j < lastColumn; j++) {
                Cell cell = row.getCell(j);
                if (cell == null) {
                    continue;
                }

                setCellValue(cell, dataRow);
            }
        }

        int lastAddedRowNumber = Integer.parseInt(templateCongigMap.get("DETAIL")) - 1 + listSize;

        sheet.removeRow(sheet.getRow(lastAddedRowNumber));
        if (lastAddedRowNumber + 1 < sheet.getLastRowNum()) {
            sheet.shiftRows(lastAddedRowNumber + 1, sheet.getLastRowNum(), -1, true, true);
        }

    }


    /**
     * 详细行的设定
     *
     * @param workbook 工作表
     * @param list     数据
     */
    public static void setBfDetail(Workbook workbook, List<Map> list) throws Exception {

        Map<String, String> templateCongigMap = getTamplateConfigMap(workbook);

        Sheet sheet = workbook.getSheetAt(0);

        int listSize = list.size();

        for (int i = 0; i < listSize; i++) {

            Map dataRow = list.get(i);

            int currentRowNumber = Integer.parseInt(templateCongigMap.get("DETAIL")) - 1 + i;

            ExcelFileUtils.copyBfRowToUnder(sheet, sheet.getRow(currentRowNumber));

            Row row = sheet.getRow(currentRowNumber);
            if (row == null) {
                continue;
            }

            int lastColumn = row.getLastCellNum();
            for (int j = 0; j < lastColumn; j++) {
                Cell cell = row.getCell(j);
                if (cell == null) {
                    continue;
                }

                setCellValue(cell, dataRow);
            }
        }

        int lastAddedRowNumber = Integer.parseInt(templateCongigMap.get("DETAIL")) - 1 + listSize;

        sheet.removeRow(sheet.getRow(lastAddedRowNumber));
        if (lastAddedRowNumber + 1 < sheet.getLastRowNum()) {
            sheet.shiftRows(lastAddedRowNumber + 1, sheet.getLastRowNum(), -1, true, true);
        }

    }


    /**
     * 模板的配置取得
     *
     * @param workbook 工作表
     * @return 模板数据
     */
    private static Map<String, String> getTamplateConfigMap(Workbook workbook) {

        Sheet sheet = workbook.getSheetAt(workbook.getSheetIndex("Config"));
        Row infoRow = sheet.getRow(0);
        Cell firstCell = infoRow.getCell(0);
        String tamplateConfig = firstCell.getStringCellValue();
        if (tamplateConfig == null || "".equals(tamplateConfig)) {
            return null;
        }
        String[] templateConfigValues = tamplateConfig.split(",");
        Map<String, String> templateConfigMap = new HashMap<>();
        for (int i = 0; i < templateConfigValues.length; i++) {
            if ("#CONFIG".equals(templateConfigValues[i])) {
                continue;
            }
            String[] keyValue = templateConfigValues[i].split(":");
            templateConfigMap.put(keyValue[0], keyValue[1]);
        }

        return templateConfigMap;
    }

    /**
     * 文件的工作表的取得
     *
     * @param fileName              文件名
     * @param excelTmplateDirectory excel模板文件的路径
     * @return 工作表
     * @throws IOException excel文件读取异常
     */
    public static Workbook getWorkbook(String fileName, String excelTmplateDirectory) throws IOException {

        try (InputStream inputStream = new FileInputStream((excelTmplateDirectory + fileName))) {
            Workbook workbook = WorkbookFactory.create(inputStream);
            return workbook;
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * 错误的工作表取得
     *
     * @param excelTmplateDirectory excel模板的路径
     * @return 工作表
     * @throws IOException 异常
     */
    public static Workbook getErrorWorkbook(String excelTmplateDirectory) throws IOException {
        try (InputStream inputStream = new FileInputStream((excelTmplateDirectory + "ErrorTamplate.xlsx"))) {
            Workbook workbook = WorkbookFactory.create(inputStream);
            return workbook;
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * 行的复制
     *
     * @param sheet
     * @param sourceRow
     */
    public static void copyRowToUnder(Sheet sheet, Row sourceRow) {

        // 行的序号的取得
        int rowPos = sourceRow.getRowNum();
        int celCount = sourceRow.getLastCellNum();

        // 指定行的最后行的，下一行设定。
        sheet.shiftRows(rowPos + 1, sheet.getLastRowNum(), 1);

        // 下一行的做成
        sheet.createRow(rowPos + 1);
        Row destRow = sheet.getRow(rowPos + 1);

        // 列的复制
        for (int i = 0; i < celCount; i++) {
            destRow.createCell(i);
            copyCell(sourceRow.getCell(i), destRow.getCell(i));
        }
    }


    /**
     * 行的复制
     *
     * @param sheet
     * @param sourceRow
     */
    public static void copyBfRowToUnder(Sheet sheet, Row sourceRow) {

        // 行的序号的取得
        int rowPos = sourceRow.getRowNum();
        int celCount = sourceRow.getLastCellNum();

        // 指定行的最后行的，下一行设定。
        sheet.shiftRows(rowPos + 1, sheet.getLastRowNum() + 1, 1);

        // 下一行的做成
        sheet.createRow(rowPos + 1);
        Row destRow = sheet.getRow(rowPos + 1);

        // 列的复制
        for (int i = 0; i < celCount; i++) {
            destRow.createCell(i);
            copyCell(sourceRow.getCell(i), destRow.getCell(i));
        }
    }


    /**
     * 列的复制
     *
     * @param fromCell 复制源
     * @param toCell   复制的列
     */
    private static void copyCell(Cell fromCell, Cell toCell) {

        if (fromCell != null) {

            int cellType = fromCell.getCellType();
            switch (cellType) {
                case Cell.CELL_TYPE_BLANK:
                    break;
                case Cell.CELL_TYPE_FORMULA:
                    String cellFormula = fromCell.getCellFormula();
                    toCell.setCellFormula(cellFormula);
                    break;
                case Cell.CELL_TYPE_BOOLEAN:
                    toCell.setCellValue(fromCell.getBooleanCellValue());
                    break;
                case Cell.CELL_TYPE_ERROR:
                    toCell.setCellErrorValue(fromCell.getErrorCellValue());
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    toCell.setCellValue(fromCell.getNumericCellValue());
                    break;
                case Cell.CELL_TYPE_STRING:
                    toCell.setCellValue(fromCell.getRichStringCellValue());
                    break;
                default:
            }

            if (fromCell.getCellStyle() != null) {
                toCell.setCellStyle(fromCell.getCellStyle());
            }

            if (fromCell.getCellComment() != null) {
                toCell.setCellComment(fromCell.getCellComment());
            }
        }
    }

    /**
     * EL样式返回
     *
     * @param cell 列
     * @return 值
     */
    private static String[] getElString(Cell cell) {
        String celData = cell.getStringCellValue().trim();

        if (celData.length() < 4) {
            return new String[]{""};
        }
        String startString = celData.substring(0, 2);
        String endString = celData.substring(celData.length() - 1, celData.length());

        if (!"${".equals(startString) || !"}".equals(endString)) {

            return new String[]{""};
        }

        String[] configString = celData.substring(2, celData.length() - 1).split(":");
        if (configString.length == 2) {
            if (!"C".equals(configString[1]) && !"M".equals(configString[1]) && !"P1".equals(configString[1]) && !"P2".equals(configString[1]) && !"P3".equals(configString[1]) && !"D".equals(configString[1]) && !"DT".equals(configString[1])) {
                return new String[]{""};
            }
        }

        return configString;
    }

    /**
     * 单元格值的设定
     *
     * @param cell    单元格
     * @param dataRow 数据
     */
    private static void setCellValue(Cell cell, Map dataRow) throws Exception {

        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {

            String[] key = getElString(cell);

            if (dataRow.containsKey(key[0])) {
                // 样式【:C】的时候的设定
                if (key.length == 2 && "C".equals(key[1])) {
                    try {
                        cell.setCellValue(parseInt(ConvertUtils.convToString(dataRow.get(key[0]))));
                    } catch (Exception e2) {
                        cell.setCellValue("");
                    }
                    // 样式【:M】的时候的设定
                } else if (key.length == 2 && "M".equals(key[1])) {
                    try {
                        cell.setCellValue(ConvertUtils.convToMoney(dataRow.get(key[0])));
                    } catch (Exception e2) {
                        cell.setCellValue("");
                    }
                    // 样式【:P1】的时候的设定
                } else if (key.length == 2 && "P1".equals(key[1])) {
                    Double data = Double.parseDouble((String) dataRow.get(key[0]));
                    cell.setCellValue(data / 10);
                    // 样式【:P2】的时候的设定
                } else if (key.length == 2 && "P2".equals(key[1])) {
                    Double data = Double.parseDouble((String) dataRow.get(key[0]));
                    cell.setCellValue(data / 100);
                    // 样式【:P3】的时候的设定
                } else if (key.length == 2 && "P3".equals(key[1])) {
                    Double data = Double.parseDouble((String) dataRow.get(key[0]));
                    cell.setCellValue(data / 10000);
                    // 样式【:D】的时候的设定
                } else if (key.length == 2 && "D".equals(key[1])) {
                    if (StringUtils.isEmpty((String) dataRow.get(key[0]))) {
                        cell.setCellValue("");
                    } else {
                        Date date = null;
                        try {
                            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                            date = format1.parse(((String) dataRow.get(key[0])).substring(0, 10));
                        } catch (Exception e) {
                            try {
                                SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd");
                                date = format2.parse(((String) dataRow.get(key[0])).substring(0, 10));
                            } catch (Exception e2) {
                                @SuppressWarnings("unused")
                                String dummy = null;
                            }
                        }
                        if (date == null) {
                            cell.setCellValue("");
                        } else {
                            cell.setCellValue(date);
                        }
                    }
                    // 样式【:DT】的时候，值的设定
                } else if (key.length == 2 && "DT".equals(key[1])) {
                    cell.setCellValue(DateUtils.convertDate2String((Date) dataRow.get(key[0]), "yyyy-MM-dd hh:mm:ss"));
                    // 没有样式的时候，直接设定值
                } else {
                    cell.setCellValue((String) dataRow.get(key[0]));
                }

            }
        }
    }
    public static int parseInt(String data) throws Exception {
        return Integer.parseInt(data.replaceAll(",", ""));
    }

    /**
     * 读取某个工作簿上的所有单元格的值。
     *
     * @param sheetOrder 工作簿序号，从0开始。
     * @return List<Object                               [                               ]>   所 有单元格的值。
     * @throws IOException            加载excel文件IO异常。
     * @throws FileNotFoundException  excel文件没有找到异常。
     * @throws InvalidFormatException
     */
    public List<Object[]> read(int sheetOrder, String path) throws FileNotFoundException,
            IOException, InvalidFormatException {
        FileInputStream fis = new FileInputStream(path);
        Workbook workbook = WorkbookFactory.create(fis);
        if (fis != null) {
            fis.close();
        }
        Sheet sheet = workbook.getSheetAt(sheetOrder);
        // 用来记录excel值
        List<Object[]> valueList = new LinkedList<Object[]>();
        // 循环遍历每一行、每一列。
        for (Row row : sheet) {
            // 每一行
            Object[] rowObject = null;
            for (Cell cell : row) {
                // cell.getCellType是获得cell里面保存的值的type
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_BOOLEAN:
                        // 得到Boolean对象的方法
                        rowObject = CollectionUtils.addObjectToArray(rowObject,
                                cell.getBooleanCellValue());
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        // 先看是否是日期格式
                        if (DateUtil.isCellDateFormatted(cell)) {
                            // 读取日期格式
                            rowObject = CollectionUtils.addObjectToArray(rowObject,
                                    cell.getDateCellValue());
                        } else {
                            DecimalFormat df = new DecimalFormat();
                            // 单元格的值,替换掉,
                            String value = df.format(cell.getNumericCellValue())
                                    .replace(",", "");
                            // 读取数字
                            rowObject = CollectionUtils.addObjectToArray(rowObject,
                                    value);
                        }
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        // 读取公式
                        rowObject = CollectionUtils.addObjectToArray(rowObject,
                                cell.getCellFormula());
                        break;
                    case Cell.CELL_TYPE_STRING:
                        // 读取String
                        rowObject = CollectionUtils.addObjectToArray(rowObject, cell
                                .getRichStringCellValue().toString());
                        break;
                }
            }
            // 将这行添加到list。
            valueList.add(rowObject);
        }
        return valueList;
    }

    /**
     * 读取某个工作簿上的某个单元格的值。
     *
     * @param sheetOrder 工作簿序号,从0开始。
     * @param colum      列数 从1开始
     * @param row        行数 从1开始
     * @return 单元格的值。
     * @throws Exception 加载excel异常。
     */
    public String read(int sheetOrder, int colum, int row, String path) throws Exception {
        FileInputStream fis = new FileInputStream(path);
        Workbook workbook = WorkbookFactory.create(fis);
        if (fis != null) {
            fis.close();
        }
        Sheet sheet = workbook.getSheetAt(sheetOrder);
        Row rows = sheet.getRow(row - 1);
        Cell cell = rows.getCell(colum - 1);
        String content = cell.getStringCellValue();
        return content;
    }

    /**
     * 在指定的工作簿、行、列书写值。
     *
     * @param sheetOrder 工作簿序号，基于0.
     * @param colum      列 基于1
     * @param row        行 基于1
     * @param content    将要被书写的内容。
     * @throws Exception 书写后保存异常。
     */
    public void write(int sheetOrder, int colum, int row, String content, String path)
            throws Exception {
        FileInputStream fis = new FileInputStream(path);
        Workbook workbook = WorkbookFactory.create(fis);
        if (fis != null) {
            fis.close();
        }
        Sheet sheet = workbook.getSheetAt(sheetOrder);
        Row rows = sheet.createRow(row - 1);
        Cell cell = rows.createCell(colum - 1);
        cell.setCellValue(content);
        FileOutputStream fileOut = new FileOutputStream(path);
        workbook.write(fileOut);
        fileOut.close();

    }

    /**
     * 得到一个工作区最后一条记录的序号，相当于这个工作簿共多少行数据。
     *
     * @param sheetOrder 工作区序号
     * @return int 序号。
     * @throws IOException            根据excel路径加载excel异常。
     * @throws InvalidFormatException
     */
    public int getSheetLastRowNum(int sheetOrder, String path) throws IOException,
            InvalidFormatException {
        FileInputStream fis = new FileInputStream(path);
        Workbook workbook = WorkbookFactory.create(fis);
        if (fis != null) {
            fis.close();
        }
        Sheet sheet = workbook.getSheetAt(sheetOrder);
        return sheet.getLastRowNum();
    }

    /**
     * 在磁盘生成一个含有内容的excel,路径为path属性
     *
     * @param sheetName 导出的sheet名称
     * @param fieldName 列名数组
     * @param data      数据组
     * @throws IOException
     */
    public void makeExcel(String sheetName, String[] fieldName,
                          List<Object[]> data, String path) throws IOException {
        // 在内存中生成工作薄
        HSSFWorkbook workbook = makeWorkBook(sheetName, fieldName, data, path);
        // 截取文件夹路径
        String filePath = path.substring(0, path.lastIndexOf("\\"));
        // 如果路径不存在，创建路径
        File file = new File(filePath);
        // System.out.println(path+"-----------"+file.exists());
        if (!file.exists())
            file.mkdirs();
        FileOutputStream fileOut = new FileOutputStream(path);
        workbook.write(fileOut);
        fileOut.close();
    }

    /**
     * 在输出流中导出excel。
     *
     * @param excelName 导出的excel名称 包括扩展名
     * @param sheetName 导出的sheet名称
     * @param fieldName 列名数组
     * @param data      数据组
     * @param response  response
     * @throws IOException 转换流时IO错误
     */
    public void makeStreamExcel(String excelName, String sheetName,
                                String[] fieldName, List<Object[]> data,
                                HttpServletResponse response, String path) throws IOException {
        OutputStream os = null;
        response.reset(); // 清空输出流
        os = response.getOutputStream(); // 取得输出流
        response.setHeader("Content-disposition", "attachment; filename="
                + new String(excelName.getBytes(), "ISO-8859-1")); // 设定输出文件头
        response.setContentType("application/msexcel"); // 定义输出类型
        // 在内存中生成工作薄
        HSSFWorkbook workbook = makeWorkBook(sheetName, fieldName, data,path);
        os.flush();
        workbook.write(os);
    }

    /**
     * 根据条件，生成工作薄对象到内存。
     *
     * @param sheetName 工作表对象名称
     * @param fieldName 首列列名称
     * @param data      数据
     * @return HSSFWorkbook
     */
    private HSSFWorkbook makeWorkBook(String sheetName, String[] fieldName,
                                      List<Object[]> data, String path) {
        // 用来记录最大列宽,自动调整列宽。
        Integer collength[] = new Integer[fieldName.length];

        // 产生工作薄对象
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 产生工作表对象
        HSSFSheet sheet = workbook.createSheet();
        // 为了工作表能支持中文,设置字符集为UTF_16
        workbook.setSheetName(0, sheetName);
        // 产生一行
        HSSFRow row = sheet.createRow(0);
        // 产生单元格
        HSSFCell cell;
        // 写入各个字段的名称
        for (int i = 0; i < fieldName.length; i++) {
            // 创建第一行各个字段名称的单元格
            cell = row.createCell((short) i);
            // 设置单元格内容为字符串型
            cell.setCellType(HSSFCell.CELL_TYPE_STRING);
            // 为了能在单元格中输入中文,设置字符集为UTF_16
            // cell.setEncoding(HSSFCell.ENCODING_UTF_16);
            // 给单元格内容赋值
            cell.setCellValue(new HSSFRichTextString(fieldName[i]));
            // 初始化列宽
            collength[i] = fieldName[i].getBytes().length;
        }
        // 临时单元格内容
        String tempCellContent = "";
        // 写入各条记录,每条记录对应excel表中的一行
        for (int i = 0; i < data.size(); i++) {
            Object[] tmp = data.get(i);
            // 生成一行
            row = sheet.createRow(i + 1);
            for (int j = 0; j < tmp.length; j++) {
                cell = row.createCell((short) j);
                // 设置单元格字符类型为String
                cell.setCellType(HSSFCell.CELL_TYPE_STRING);
                tempCellContent = (tmp[j] == null) ? "" : tmp[j].toString();
                cell.setCellValue(new HSSFRichTextString(tempCellContent));

                // 如果自动调整列宽度。
                if (autoColumnWidth) {
                    if (j >= collength.length) { // 标题列数小于数据列数时。
                        collength = CollectionUtils.addObjectToArray(collength,
                                tempCellContent.getBytes().length);
                    } else {
                        // 如果这个内容的宽度大于之前最大的，就按照这个设置宽度。
                        if (collength[j] < tempCellContent.getBytes().length) {
                            collength[j] = tempCellContent.getBytes().length;
                        }
                    }
                }
            }
        }

        // 自动调整列宽度。
        if (autoColumnWidth) {
            // 调整列为这列文字对应的最大宽度。
            for (int i = 0; i < fieldName.length; i++) {
                sheet.setColumnWidth(i, collength[i] * 2 * 256);
            }
        }
        return workbook;
    }

    /**
     * 功能：设置写入excel时，是否自动扩展列宽度来符合内容，默认为false。
     *
     * @param autoColumnWidth true或者false
     */
    public void setAutoColumnWidth(boolean autoColumnWidth) {
        this.autoColumnWidth = autoColumnWidth;
    }
}
