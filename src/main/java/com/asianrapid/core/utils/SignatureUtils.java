package com.asianrapid.core.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Map;

public class SignatureUtils {
//	public static String signRequest(Map<String, String> params, String secret)
//			throws IOException {
//		// 第一步：检查参数是否已经排序
//		String[] keys = params.keySet().toArray(new String[0]);
//		Arrays.sort(keys);
//
//		// 第二步：把所有参数名和参数值串在一起
//		StringBuilder query = new StringBuilder();
//		query.append(secret);
//
//		for (String key : keys) {
//			String value = params.get(key);
////			if (Validator.isValid(key) && Validator.isValid(value)) {
////				query.append(key).append(value);
////			}
//		}
//		query.append(secret);
//
//		// 第三步：使用MD5, 把二进制转化为大写的十六进制
//		return Md5Util.encodeMd5(query.toString().getBytes("UTF-8")).toUpperCase();
//	}
//
//	public static byte[] encryptMD5(String data) throws UnsupportedEncodingException {
//		return Md5Util.encodeMd5(data.getBytes("UTF-8")).getBytes("UTF-8");
//	}

	public static String byte2hex(byte[] bytes) {
		StringBuilder sign = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(bytes[i] & 0xFF);
			if (hex.length() == 1) {
				sign.append("0");
			}
			sign.append(hex.toUpperCase());
		}
		return sign.toString();
	}
}
