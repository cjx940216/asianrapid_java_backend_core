package com.asianrapid.core.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description:
 */
public class LocalCache {


	@Autowired
	private CacheManager cacheManager;

	@Transactional(propagation = Propagation.SUPPORTS)
	public Object get(CacheType type, String key) {

		Cache cache = cacheManager.getCache(type.value);
		Object obj = cache.get(key) == null ? null : cache.get(key).get();

		return obj;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	public Object put(CacheType type, String key, Object obj) {

		Cache cache = cacheManager.getCache(type.value);
		cache.put(key, obj);

		return obj;
	}

	public enum CacheType {
		WX_SHARE("wx.share"),
		STATICCTX("staticCtx");

		/**
		 * 取得値
		 */
		private String value;

		/**
		 * Constructor.
		 *
		 * @param value
		 */
		private CacheType(String value) {
			this.value = value;
		}
	}
}
