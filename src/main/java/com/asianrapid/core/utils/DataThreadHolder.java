package com.asianrapid.core.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 16:00
 * @param <T>
 * @Description: 线程数据容器
 */
public class DataThreadHolder<T> {
	private Map<Thread, T> valueMap	= Collections.synchronizedMap(new HashMap<Thread, T>());

	private volatile static DataThreadHolder<?>	instance = null;

	private DataThreadHolder() {
	}

	public static <S> DataThreadHolder<S> getInstance() {
		if (instance == null) {
			synchronized (DataThreadHolder.class) {
				if (instance == null) {
					instance = new DataThreadHolder<S>();
				} else {
				}
			}
		} else {
		}
		return (DataThreadHolder<S>) instance;
	}

	public void set(T newValue) {

		// 键为线程对象，值为本线程的变量副本
		valueMap.put(Thread.currentThread(), newValue);
	}

	public T get() {

		return valueMap.get(Thread.currentThread());// 返回本线程对应的变量
	}

	public void remove() {

		valueMap.remove(Thread.currentThread());

	}
}
