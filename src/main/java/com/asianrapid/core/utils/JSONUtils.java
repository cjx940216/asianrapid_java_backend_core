package com.asianrapid.core.utils;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Version: 1.0
 * @Description: JSON工具类
 */
public class JSONUtils {
	/**
	 * 将对象序列化为json字符串
	 *
	 * @param obj
	 *            需要被序列化的内容
	 * @return 序列化后的字符串
	 */
	public static String parseJson(Object obj) {
		return JSON.toJSONString(obj);
	}

	/**
	 * 将json字符串反序列化为对象
	 *
	 * @param text
	 *            需要反序列化的json字符串
	 * @param clazz
	 *            需要反序列化成的类
	 * @return 反序列化之后的对象
	 */
	public static <T> T parseObject(String text, Class<T> clazz) {
		return JSON.parseObject(text, clazz);
	}

	/**
	 * 将json字符串反序列化为List对象
	 *
	 * @param text
	 *            需要反序列化的json字符串
	 * @param clazz
	 *            需要反序列化成的List中的范型
	 * @return 反序列化之后的数组对象
	 */
	public static <T> List<T> parseArray(String text, Class<T> clazz) {
		return JSON.parseArray(text, clazz);
	}
}
