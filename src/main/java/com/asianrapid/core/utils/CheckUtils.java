package com.asianrapid.core.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 16:00
 * @Description: 校验数据工具类
 */
public class CheckUtils {

    /**
     * 验证邮箱地址是否正确
     * @param email 传入字符串类型的邮箱地址
     * @return true 邮箱地址正确 false 邮箱地址不正确
     */
    public static boolean isEmail(String email) {
        boolean flag = false;
        try {
            String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
            Pattern regex = Pattern.compile(check);
            Matcher matcher = regex.matcher(email);
            flag = matcher.matches();
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }

    /**
     * 根据身份证判断男女
     * @param id_card 字符类型的身份证号
     * @return int 1:男 0:女
     * */
    public static int sexForCard(String id_card) {
        if (id_card.length() == 18) {// 如果是18位身份证
            int i = Integer.parseInt(id_card.substring(16, 17));
            if (i % 2 == 0) {
                return 0;
            } else {
                return 1;
            }
        } else {// 如果是15位身份证
            int i = Integer.parseInt(id_card.substring(14, 15));
            if (i % 2 == 0) {
                return 0;
            } else {
                return 1;
            }
        }
    }


}
