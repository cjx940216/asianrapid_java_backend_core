package com.asianrapid.core.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description: Redis操作工具类
 */
@Repository
public class RedisDaoUtils {

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 添加一组键值对数据到缓存中
     * @param key
     * @param value
     */
    public void setKey(String key,String value){
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        ops.set(key,value);
    }

    /**
     * 添加一组键值对数据到缓存中,设置过期时间单位秒
     * @param key
     * @param value
     * @param time 过期时间秒
     */
    public void setKey(String key,String value, int time){
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        ops.set(key,value,time,TimeUnit.SECONDS);
    }

    /**
     * 添加一组键值对数据到缓存中,设置过期时间
     * @param key
     * @param value
     * @param time 过期时间
     * @param timeUnit 时间类型
     */
    public void setKey(String key,String value, int time, TimeUnit timeUnit){
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        ops.set(key,value,time,timeUnit);
    }

    /**
     * 通过key值获取value
     * @param key
     * @return
     */
    public String getValue(String key){
        ValueOperations<String, String> ops = this.redisTemplate.opsForValue();
        return ops.get(key);
    }

    /**
     * 通过key值获取值的过期时间 秒
     * @param key
     * @return
     */
    public Long getExpire(String key){
        return this.redisTemplate.getExpire(key,TimeUnit.SECONDS);
    }

    /**
     * 通过key值获取过期时间,指定时间类型
     * @param key
     * @param timeUnit 时间类型
     * @return
     */
    public Long getExpire(String key, TimeUnit timeUnit){
        return this.redisTemplate.getExpire(key,timeUnit);
    }

    /**
     * 删除缓存中存储的键值对
     * @param key
     * @return
     */
    public void deleteValue(String key){
        this.redisTemplate.delete(key);
    }

    /**
     * 检查key是否存在，返回boolean值
     * @param key
     * @return
     */
    public Boolean hasKey(String key){
        return this.redisTemplate.hasKey(key);
    }

    /**
     * 向指定key中存放set集合
     * @param key
     * @param list 需要存放的数据
     * @return
     */
    public Long addSet(String key, String... list){
        return this.redisTemplate.opsForSet().add(key, list);
    }

    /**
     * 根据key查看集合中是否存在指定数据
     * @param key
     * @param value
     * @return
     */
    public Boolean isMember(String key, String value){
        return this.redisTemplate.opsForSet().isMember(key, value);
    }

    /**
     * 根据key获取set集合
     * @param key
     * @return
     */
    public Set<String> members(String key){
        return this.redisTemplate.opsForSet().members(key);
    }
}