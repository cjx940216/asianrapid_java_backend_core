package com.asianrapid.core.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description:URL工具类
 */
public class URLUtils {

    /**
     * URL 转码为utf-8 加密
     * URLEncoder类包含一个encode(String s,String charcter)静态方法,<br>
     * 它可以将普通字符串转换成application/x-www-form-urlencoded MIME字符串
     * @param str 加密后的url字符串
     * @return 转码为utf-8的字符串
     */
    public static String getURLEncoderString(String str) {
        String result = "";
        if (null == str) {
            return "";
        }
        try {
            result = java.net.URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * URL参数按照指定的编码变换。<br> 加密
     * URLEncoder类包含一个encode(String s,String charcter)静态方法,<br>
     * 它可以将普通字符串转换成application/x-www-form-urlencoded MIME字符串
     * @param str 要变换的字符串
     * @param enc 指定的编码
     * @return 转换后的字符串
     * */
    public static String encode(String str, String enc) {
        try {
            return URLEncoder.encode(str, enc);
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }

    /**
     * URL 转码为utf-8 解密
     * URLDecoder类包含一个decode(String s,String charcter)静态方法,<br>
     * 它可以将看上去乱码的特殊字符串转换成普通字符串
     * @param str 加密后的url字符串
     * @return 转码为utf-8的字符串
     */
    public static String decode(String str){
        return decode(str, "UTF-8");
    }

    /**
     * URL参数按照指定的编码转码。<br> 解密
     * URLDecoder类包含一个decode(String s,String charcter)静态方法,<br>
     * 它可以将看上去乱码的特殊字符串转换成普通字符串
     * @param str 要变换的字符串
     * @param enc 指定的编码
     * @return 转换后的字符串
     * */
    public static String decode(String str, String enc) {
        try {
            return URLDecoder.decode(str, enc);
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }
}
