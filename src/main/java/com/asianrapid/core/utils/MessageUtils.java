package com.asianrapid.core.utils;

import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description:使用这个类的时候需要在resource文件夹中定义一个message资源文件
 */
public class MessageUtils {

	private static ResourceBundle bundle = ResourceBundle.getBundle("message");

	private static MessageUtils instance = new MessageUtils();

	public static MessageUtils getInstance() {
		return instance;
	}

	/**
	 * 业务方面的message信息取得。
	 * 将信息按照message.properties中的格式输出
	 *    (message.properties)
	 *
	 * @param key messageKey 传递的参数的格式必须是 {0} {1} {2} {3}这种格式
	 * @param params message参数
	 * @return message
	 */
	public static String getMessage(String key, Object... params) {
		return MessageFormat.format(bundle.getString(key), params);
	}
}
