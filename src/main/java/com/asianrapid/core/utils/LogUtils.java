package com.asianrapid.core.utils;

import com.asianrapid.core.enums.LogTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description: 记录日志工具类
 */
public class LogUtils {

	/**
	 * 记录log日志
     * @param clz 需要记录日志的class对象
	 * @param logMsg 日志的信息
     * @param logTypeEnum 日志的类型
	 */
	public static void saveLog(Class clz, String logMsg, LogTypeEnum logTypeEnum) {
		if(StringUtils.isEmpty(logMsg)) {
			return;
		}
        Logger logger = LoggerFactory.getLogger(clz);
		switch (logTypeEnum) {
		case DEBUG:
			logger.debug(logMsg);
			break;
		case INFO:
			logger.info(logMsg);
			break;
		case WARING:
			logger.warn(logMsg);
			break;
		case ERROR:
			logger.error(logMsg);
			break;
		default:
			logger.debug(logMsg);
			break;
		}
		System.out.print(logMsg);
	}

}
