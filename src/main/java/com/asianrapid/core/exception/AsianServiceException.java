package com.asianrapid.core.exception;

import com.asianrapid.core.enums.ExceptionEnum;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description:服务（业务）异常如“ 账号或密码错误 ”
 */
public class AsianServiceException extends Exception {

	private static final long serialVersionUID = 5920387266123214669L;

	private String code;

	private String detailMessage;
	
	private Object detailData;

	public AsianServiceException() {
	}

	public AsianServiceException(String message) {
		super(message);
	}

	public AsianServiceException(String code, String message) {
		super(message);
		this.code = code;
	}

	public AsianServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public AsianServiceException(ExceptionEnum iwe) {
		super(iwe.getMessage());
		this.code = iwe.getCode();
	}

	public AsianServiceException(ExceptionEnum iwe, String detailMessage) {
		super(iwe.getMessage());
		this.code = iwe.getCode();
		this.detailMessage = detailMessage;
	}
	
	public AsianServiceException(ExceptionEnum iwe, Object detailData) {
		super(iwe.getMessage());
		this.code = iwe.getCode();
		this.detailData = detailData;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDetailMessage() {
		return detailMessage;
	}

	public void setDetailMessage(String detailMessage) {
		this.detailMessage = detailMessage;
	}

	public Object getDetailData() {
		return detailData;
	}

	public void setDetailData(Object detailData) {
		this.detailData = detailData;
	}
}
