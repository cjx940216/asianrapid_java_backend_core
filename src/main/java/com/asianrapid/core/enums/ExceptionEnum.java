package com.asianrapid.core.enums;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/19 13:42
 * @Description:api结果返回状态码枚举类
 */
public enum ExceptionEnum {
    /* E0000 -- E0099 开放平台使用 */
    E0001(""), E0002(""), // 保留占用
    E0003("service fault"), E0004("invalid primary key"), // 数据主键或数据索引无效
    E0005("Duplicate primary key"), E0006("因数据是关联表的外键无法删除"), // 因数据是关联表的外键无法删除
    E0007("服务端通信异常，请联系管理员处理。"), E0008("missing server"), E0009("Invalid signature"), E0010("Invalid APPKEY"), E0011(
            "Request parameters are incomplete")
    ,E0012("无法使用系统配置的信息连接数据库。")
    ,E0013("因用户访问权限不足无法获得视图的基本信息。")
    ,E0014("因格式错误导致无法导入数据。")
    ,
    /* 数据导入 导出功能 */
    E0101(""), E0102("缺少必要的数据内容"), E0103("数据列名与数据集不匹配，请检查配置。")
    ,E0104("数据集配置错误导致无创建运行实例，请检查配置。")
    ,E0105("导入数据未能通过逻辑检查，请检查数据内容。")
    ,E0106("导出数据集定义与表或视图结构不符，请检查配置内容。")
    /* E010001 --- E019999 外部接口类错误 */
    ,E010001(""), E020001(""), E040001(""),
    /*修改密码*/
    E050001("原密码输入错误"),E050002("修改密码失败：数据库访问出错，请联系管理员"),
    /*功能选择*/
    E060001("禁用失败：数据库访问出错，请联系管理员"),E060002("启用失败：数据库访问出错，请联系管理员"),
    E060003("数据内容不符合格式要求"),

    /**
     * 专属平台
     */
    E110001(""), E110002(""), E110003(""),
    E110010("企业法人手机号不存在。"),
    E110011("非系统用户无法登陆，如有需要请联系管理员建立用户。")
    ;

    private String code;

    private String message;

    ExceptionEnum(String message) {
        this.message = message;
    }

    ExceptionEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
