package com.asianrapid.core.enums;

/**
 * @Auther: 迟家鑫
 * @Date: 2018/6/27 09:54
 * @Description: 日志级别枚举类
 */
public enum LogTypeEnum {
    DEBUG,
    INFO,
    WARING,
    ERROR;

    LogTypeEnum(){

    }
}
